from spyne import Application, rpc, ServiceBase
from spyne.server.wsgi import WsgiApplication
from spyne.protocol.soap import Soap11
from spyne import Float, String
from wsgiref.simple_server import make_server
import time

class SOAPService(ServiceBase):
    @rpc(Float, Float, Float, _returns=String) 
    def Trajet(ctx, t_trajet, nb_arret, t_chargement):
        t_total_seconds = t_trajet+(nb_arret*(t_chargement*60))
        t_total = time.strftime("%Hh%M", time.gmtime(t_total_seconds))
        t_trajet = time.strftime("%Hh%M", time.gmtime(t_trajet))
        return t_total
            
application = Application([SOAPService], 'spyne.examples.hello.soap',
                          in_protocol=Soap11(validator='lxml'),
                          out_protocol=Soap11())
wsgi_application = WsgiApplication(application)

server = make_server('127.0.0.1', 8000, wsgi_application)
server.serve_forever()