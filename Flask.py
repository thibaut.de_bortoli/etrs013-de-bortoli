# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 11:37:40 2023

@author: user
"""
from flask import Flask, render_template, request
from zeep import Client
from geopy.geocoders import Nominatim
import geopy.distance
import requests
import folium
import pandas as pd
from math import ceil
import time

def get_vehicles():
    # Remplacez ces valeurs par vos identifiants
    CLIENT_ID = '6545016fe58e2dfeb5fb688f'
    APP_ID = '6545016fe58e2dfeb5fb6891'

    # URL de l'API GraphQL Chargetrip
    API_ENDPOINT = 'https://api.chargetrip.io/graphql'

    # URL de l'API GraphQL Chargetrip
    API_ENDPOINT = 'https://api.chargetrip.io/graphql'

    # Définissez une requête GraphQL avec des sous-champs spécifiés pour les champs nécessaires
    graphql_query = '''
    query vehicleListAll {
      vehicleList {
        naming {
          make
          model
          chargetrip_version
        }
        drivetrain {
          type
        }
        connectors {
          standard
          power
          max_electric_power
          time
          speed
        }
        adapters {
          standard
          power
          max_electric_power
          time
          speed
        }
        battery {
          usable_kwh
          full_kwh
        }
        body {
          seats
        }
        availability {
          status
        }
        range {
          chargetrip_range {
            best
            worst
          }
        }
        media {
          image {
            id
            type
            url
            height
            width
            thumbnail_url
            thumbnail_height
            thumbnail_width
          }
          brand {
            id
            type
            url
            height
            width
            thumbnail_url
            thumbnail_height
            thumbnail_width
          }
        }
      }
    }

    '''

    # Créez un dictionnaire pour les en-têtes de votre requête
    headers = {
        'Content-Type': 'application/json',
        'x-client-id': CLIENT_ID,
        'x-app-id': APP_ID,
    }

    # Envoyez la requête HTTP à l'API Chargetrip
    response = requests.post(API_ENDPOINT, json={'query': graphql_query}, headers=headers)       
    data = response.json()
    vehicles = data['data']['vehicleList']
    vehicles_by_make = {}
    for vehicle in vehicles:
        make = vehicle['naming']['make']
        # Si la marque n'existe pas encore dans le dictionnaire, créez une liste vide
        if make not in vehicles_by_make:
            vehicles_by_make[make] = []
    
        # Ajoutez le véhicule à la liste correspondante
        vehicles_by_make[make].append(vehicle)
    return vehicles_by_make


def get_bornes_elec():
    url = "https://odre.opendatasoft.com/api/explore/v2.1/catalog/datasets/bornes-irve/exports/geojson?where=xlongitude%20is%20not%20null%20and%20ylatitude%20is%20not%20null&limit=-1&timezone=UTC&use_labels=false&epsg=4326"
    response = requests.request("GET", url)
    df = pd.DataFrame(columns = ['Name', 'Address', 'Point'])
    for borne in response.json()['features']:
        try:
            name = borne['properties']['n_station']
            address = borne['properties']['ad_station']
            point = [borne['geometry']['coordinates'][1],borne['geometry']['coordinates'][0]]
            tmp_df = pd.DataFrame([[name, address, point]], columns=['Name', 'Address', 'Point'])
            df = pd.concat([df, tmp_df], ignore_index=True)
        except:
            print('')
    df = df.drop_duplicates(subset=['Point'])
    return df
    
def get_arrets_bornes(response_direction, response_bornes, autonomie):
    mls = response_direction.json()['features'][0]['geometry']['coordinates']
    list_coord_direction = [[i[1], i[0]] for i in mls[0]]
    list_coord_borne = response_bornes.Point
    c = d = distance_total_troncon = 0
    list_point_arret_autonomie = []
    list_point_final = []
    list_point_final.append(list_coord_direction[0])
    distance_total = (response_direction.json()['features'][0]['properties']['distance'])/1000
    nb_arret = ceil(distance_total/autonomie)-1
    
    for i in range(nb_arret):
        while distance_total_troncon < 0.90*autonomie and d != len(list_coord_direction):
            distance_total_troncon += geopy.distance.geodesic(list_coord_direction[c], list_coord_direction[d]).km
            c=d
            d+=1
        list_point_arret_autonomie.append(list_coord_direction[c])
        distance_total_troncon = 0    
    
    for point_arret in list_point_arret_autonomie:
        closest_borne = None
        min_distance = float('inf')
        # Triez les bornes par distance par rapport au point d'arrêt actuel
        sorted_bornes = sorted(list_coord_borne, key=lambda coord_borne: geopy.distance.geodesic(point_arret, coord_borne).km)
        closest_bornes = sorted_bornes[:2]

                  
        # Parcourez la liste de coordonnées et formatez-les
        for coord in closest_bornes:
                        
            formatted_string = f"{point_arret[0]},{point_arret[1]}|{list_point_final[-1][0]},{list_point_final[-1][1]}"

            mode= "drive"
            url = "https://route-and-directions.p.rapidapi.com/v1/routing"
            key = "8283d6c95dmshe7c4e240dacafd4p14fde3jsne75c0f77ddd0"
            host = "route-and-directions.p.rapidapi.com"
            headers = {"X-RapidAPI-Key": key, "X-RapidAPI-Host": host}
            querystring = {"waypoints":formatted_string,"mode":mode}
            response = requests.request("GET", url, headers=headers, params=querystring)
            distance = len(response.json()['features'][0]['geometry']['coordinates'][0])
            
            if distance < min_distance:
                min_distance = distance
                closest_borne = coord
        
        list_point_final.append(closest_borne)
    
    list_point_final.append(list_coord_direction[-1])
    return list_point_final, list_point_arret_autonomie, nb_arret  

    
def get_lat_long_from_address(address):
   locator = Nominatim(user_agent='tibidebortolo')
   location = locator.geocode(address)
   return location.latitude, location.longitude

def get_directions_response(lat1, long1, lat2, long2, mode='drive'):
   url = "https://route-and-directions.p.rapidapi.com/v1/routing"
   key = "8283d6c95dmshe7c4e240dacafd4p14fde3jsne75c0f77ddd0"
   host = "route-and-directions.p.rapidapi.com"
   headers = {"X-RapidAPI-Key": key, "X-RapidAPI-Host": host}
   querystring = {"waypoints":f"{str(lat1)},{str(long1)}|{str(lat2)},{str(long2)}","mode":mode}
   response = requests.request("GET", url, headers=headers, params=querystring)
   return response

def create_map(response_point_final, response_arret_autonomie):
   mode= "drive"
   url = "https://route-and-directions.p.rapidapi.com/v1/routing"
   key = "8283d6c95dmshe7c4e240dacafd4p14fde3jsne75c0f77ddd0"
   host = "route-and-directions.p.rapidapi.com"
   headers = {"X-RapidAPI-Key": key, "X-RapidAPI-Host": host}
   
  # Initialisation d'une liste pour stocker les coordonnées formatées
   formatted_coordinates = []
    
   # Parcourez la liste de coordonnées et formatez-les
   for coord in response_point_final:
       formatted_coord = f"{coord[0]},{coord[1]}"
       formatted_coordinates.append(formatted_coord)
    
   # Convertissez la liste de coordonnées formatées en une chaîne de caractères en utilisant le caractère "|"
   formatted_string = "|".join(formatted_coordinates) 
   
   querystring = {"waypoints":formatted_string,"mode":mode}
   response = requests.request("GET", url, headers=headers, params=querystring)
      
   m = folium.Map()
       
   for point in response_point_final:
       if point == response_point_final[0] or point == response_point_final[-1]:
           folium.Marker(point,icon=folium.Icon(color="blue", icon='glyphicon glyphicon-flag')).add_to(m)
       else:
           folium.Marker(point,icon=folium.Icon(color="green", icon='glyphicon glyphicon-leaf')).add_to(m)
       
#   for point in response_arret_autonomie:
#       folium.Marker(point,icon=folium.Icon(color="red", icon='glyphicon glyphicon-warning-sign')).add_to(m)
   
   mls = response.json()['features'][0]['geometry']['coordinates']
   # Initialisez une nouvelle liste vide pour la fusion des sous-listes
   merged_list = []

   # Parcourez les sous-listes et ajoutez leurs éléments à la liste fusionnée
   for sublist in mls:
       merged_list.extend(sublist)
   
    
   distance_total = response.json()['features'][0]['properties']['distance']/1000
   temps_trajet_seconds = response.json()['features'][0]['properties']['time']
   
   points = [(i[1], i[0]) for i in merged_list]
   # add the lines
   folium.PolyLine(points, weight=5, opacity=1).add_to(m)   # create optimal zoom
   df = pd.DataFrame(mls[0]).rename(columns={0:'Lon', 1:'Lat'})[['Lat', 'Lon']]
   sw = df[['Lat', 'Lon']].min().values.tolist()
   ne = df[['Lat', 'Lon']].max().values.tolist()
   m.fit_bounds([sw, ne])
   return m, distance_total, temps_trajet_seconds

app = Flask(__name__)
response_bornes = get_bornes_elec()
response_vehicles = get_vehicles()

@app.route('/', methods=('GET', 'POST'))
def index():
    m=folium.Map(location=(45.899247, 6.129384))
    return render_template('index.html',m=m._repr_html_(), vehicles=response_vehicles)

@app.route('/route', methods=('GET', 'POST'))
def route():
    if request.method=='POST':
        data = request.form
        depart = data['PointDeDepart']
        arrive = data['Destination']
        minRange = float(data['minRangeInfo'])
        maxRange = float(data['maxRangeInfo'])
        
        averageRange = (minRange+maxRange)/2
        
        coord_depart = get_lat_long_from_address(depart)
        coord_arrive = get_lat_long_from_address(arrive)
        
        response_direction = get_directions_response(coord_depart[0], coord_depart[1], coord_arrive[0], coord_arrive[1])
        
        response_point_final, response_arret_autonomie, nb_arret = get_arrets_bornes(response_direction, response_bornes, averageRange)
        
        m, distance_total, temps_trajet_seconds = create_map(response_point_final, response_arret_autonomie)
        
        client_soap = Client('http://127.0.0.1:8000/?wsdl')
        temps_total = client_soap.service.Trajet(temps_trajet_seconds, nb_arret, 20)
        
    return render_template('index.html',m=m._repr_html_(), vehicles=response_vehicles, distance_total=distance_total, temps_total=temps_total)

if __name__ == "__main__":
        app.run()